import { IUser } from 'app/core/user/user.model';

export interface IMensaje {
    id?: number;
    titulo?: string;
    descripcion?: string;
    datosContentType?: string;
    datos?: any;
    user?: IUser;
}

export class Mensaje implements IMensaje {
    constructor(
        public id?: number,
        public titulo?: string,
        public descripcion?: string,
        public datosContentType?: string,
        public datos?: any,
        public user?: IUser
    ) {}
}
