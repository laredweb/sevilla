import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IMensaje } from 'app/shared/model/mensaje.model';

@Component({
    selector: 'jhi-mensaje-detail',
    templateUrl: './mensaje-detail.component.html'
})
export class MensajeDetailComponent implements OnInit {
    mensaje: IMensaje;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ mensaje }) => {
            this.mensaje = mensaje;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
