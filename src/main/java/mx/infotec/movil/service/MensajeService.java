package mx.infotec.movil.service;

import mx.infotec.movil.domain.Mensaje;
import mx.infotec.movil.repository.MensajeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Mensaje.
 */
@Service
@Transactional
public class MensajeService {

    private final Logger log = LoggerFactory.getLogger(MensajeService.class);

    private final MensajeRepository mensajeRepository;

    public MensajeService(MensajeRepository mensajeRepository) {
        this.mensajeRepository = mensajeRepository;
    }

    /**
     * Save a mensaje.
     *
     * @param mensaje the entity to save
     * @return the persisted entity
     */
    public Mensaje save(Mensaje mensaje) {
        log.debug("Request to save Mensaje : {}", mensaje);
        return mensajeRepository.save(mensaje);
    }

    /**
     * Get all the mensajes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Mensaje> findAll() {
        log.debug("Request to get all Mensajes");
        return mensajeRepository.findAll();
    }


    /**
     * Get one mensaje by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Mensaje> findOne(Long id) {
        log.debug("Request to get Mensaje : {}", id);
        return mensajeRepository.findById(id);
    }

    /**
     * Delete the mensaje by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Mensaje : {}", id);
        mensajeRepository.deleteById(id);
    }
}
