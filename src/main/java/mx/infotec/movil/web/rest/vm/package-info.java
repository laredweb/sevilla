/**
 * View Models used by Spring MVC REST controllers.
 */
package mx.infotec.movil.web.rest.vm;
